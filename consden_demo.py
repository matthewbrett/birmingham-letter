""" Demonstration of consden-related stuff
"""

import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt

import nibabel as nib
from nipy.algorithms.diagnostics.timediff import time_slice_diffs
from nipy.algorithms.diagnostics.tsdiffplot import plot_tsdiffs

img = nib.load('ds114_sub009_t2r1.nii')
data = img.get_data()

# Knock off first four scans
n_dummies = 4
data = data[..., n_dummies:]
n_scans = data.shape[-1]

# Do tsdiffana diagnostics
ts_res = time_slice_diffs(data, time_axis=3, slice_axis=2)
plt.figure(figsize=(10, 8))
axes = [plt.subplot(4, 1, i+1) for i in range(4)]
plot_tsdiffs(ts_res, axes)
plt.savefig('tsdiffana_example.png')
plt.close()

# Brain mask from mean
mean_data = data.mean(axis=-1)

from skimage.filters import threshold_otsu
thresh = threshold_otsu(mean_data)
mask = mean_data > thresh

# Get in-brain time courses
vox_by_time = data[mask]
vox_by_time.shape

# Make discrete cosine basis set
# See https://en.wikipedia.org/wiki/Discrete_cosine_transform#DCT-II
N = n_scans
n = np.arange(n_scans)
n_plus_half = n + 0.5
dct_basis = np.ones((N, 8))
for k in range(0, 8):
    dct_basis[:, k] = np.cos(np.pi / N * n_plus_half * k)
plt.imshow(dct_basis, aspect=0.1)
plt.savefig('dct_basis.png')
plt.close()

# Make HRF
from stimuli import events2neural
from spm_funcs import spm_hrf
TR = img.header['pixdim'][4]  # Sometimes this is not set correctly
neural = events2neural('ds114_sub009_t2r1_cond.txt', TR, img.shape[-1])
hrf_times = np.arange(0, 30, TR)
hrf_samples = spm_hrf(hrf_times)
hemo = np.convolve(neural, hrf_samples)[:len(neural)]

# Make the original design
X_o = np.hstack((hemo[n_dummies:, None], dct_basis))

# Design figure
fig, (d_ax, hrf_ax) = plt.subplots(1, 2, figsize=(10, 7))
d_ax.imshow(X_o, aspect=1/10., cmap='gray')
d_ax.set_ylabel('Time (scans)')
d_ax.set_xlabel('Regressors')
hrf_ax.plot(hemo)
hrf_ax.set_xlabel('Time (scans)')
hrf_ax.set_ylabel('Predicted signal')
plt.savefig('design_illustration.png')
plt.close()

# Project out the design
time_by_vox = vox_by_time.T
B_o = npl.pinv(X_o).dot(time_by_vox)
filtered_t_v = time_by_vox - X_o.dot(B_o)

# Do plot similar to Power et al 2013
def clip(array, percentiles=(0.5, 99.5)):
    pct_lo, pct_hi = np.percentile(array, percentiles)
    return np.clip(array, pct_lo, pct_hi)

plt.figure(figsize=(10, 3))
plt.imshow(clip(filtered_t_v), aspect=50)
plt.xlabel('Voxels')
plt.ylabel('Time (scans)')
plt.savefig('powers_plot.png')
plt.close()

# Per-scan design
X_c = np.eye(n_scans)
plt.figure(figsize=(10, 3))
plt.imshow(X_c, cmap='gray', aspect=0.3)
plt.title = "$X_c$"
plt.ylabel = 'Time (scans)'
plt.xlabel = 'Regressors'
plt.savefig('x_c.png')
plt.close()

# With per-design scan
X_a = np.hstack((X_o, X_c))
plt.figure(figsize=(10, 3))
plt.imshow(X_a, cmap='gray', aspect=0.3)
plt.title = "$X_a$"
plt.ylabel = 'Time (scans)'
plt.xlabel = 'Regressors'
plt.savefig('x_a.png')
plt.close()
