default: slides-bham

all: slides-bham

slides-%:
	pandoc -t beamer -s $*_slides.md -o $*_slides.pdf

job-description:
	pandoc -t latex -s job_description.md -o mb_job_description.pdf
