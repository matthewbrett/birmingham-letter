My initial education was in experimental psychology and medicine.  I trained
to registrar level as a neurologist, before starting my full-time research
career in 1996.

I first worked with John Stein in Oxford and David Brooks in the Hammersmith
Hospital, using PET to study the mechanisms of apraxia, a common disorder of
higher-order motor control.  I found that my colleagues, like me, had little
training in analysis and interpretation of imaging data.  I began to research
and teach on imaging methods; I wrote some popular online tutorials on imaging
and statistics (see CV).

My next position was in the MRC CBU, working with John Duncan and other CBU
researchers on motor and cognitive neuroscience, and imaging methods.  I
organized research and teaching seminars across the Cambridge community and
set up analysis methods, including contributions to the standard SPM software,
and writing a widely-used tool for the analysis of regions of interest data
(see CV).  I wrote methods papers and review articles that have been widely
cited (h-index 31, i10-index 37).  I was one of six authors on the standard
guidelines for reporting an FMRI study (Poldrack et al 2008).

A 2 year visit to Berkeley from 2003 gave me more training in motor
neuroscience and put me in contact with the developing movements of
reproducible science and formal training in research computing [1].  I
co-founded the Python in Neuroimaging group [2], and co-led a P20 grant
application for software development and training in neuroimaging, later
funded as an RO1 grant.

I returned to the MRC CBU as a tenured senior investigator scientist, but
continued my collaborations at Berkeley, and returned to Berkeley in 2008 to
concentrate on that work.

There has been an increasing emphasis on reproducible research and training in
research computing, across the Berkeley campus and beyond, latterly under the
umbrella of "data science" [3].  As I have gained experience in these methods,
I have worried more about the level of training we are giving to imaging
researchers, and the quality of work we are producing.  My strong impression
has been that the new students and post-docs in the field have less background
in basics such as physiology, anatomy, mathematics and statistics, but are
more likely to use complex analysis techniques such as multi-voxel pattern
analysis and graph theoretic measures of connectivity.  I would be very
surprised if this was not causing an epidemic of false positive and
uninterpretable results [4].

I have tried to address these problems by working on two fronts.  First, I
have continued my work with a community of psychologists, computer scientists,
statisticians and engineers to build a library of simple, clearly written
tools to make it easier for researchers to understand their existing analyses
and build new ones.  I am project leader for 3 major imaging software
projects, and a major contributor to a standard toolbox for diffusion imaging.
Second, with colleagues at Berkeley, I have designed and taught new methods of
teaching imaging and statistics that focus on understanding through doing, and
reproducible research methods - e.g. [5] (for the Statistics department) and
[6] (Psychology).

I believe this combination of approaches is essential for training the next
generation of researchers. From my reading and experience, I am confident that
the institution that leads in modern research practice will have a large
advantage over its competitors.  Reproducible research methods are more
efficient and make it easier to collaborate.  The results are more more likely
to be correct and are easier to replicate, and so form a solid foundation for
future research.

It is no coincidence that centres such as the FIL and FMRIB have been
successful in promoting research in their home institutions.  These centres
develop their own software, so researchers can discuss their questions with
the people writing the algorithms.  This gives researchers better
understanding of algorithms, and methodologists better understanding of
research.  The software projects I work on are, by design, international and
open.  The open model has the advantage of attracting developers from many
labs and backgrounds.  It is also an excellent vehicle for collaboration.  As
the central developer to a major set of open imaging software tools, I will
strengthen imaging research in Birmingham, and make it easier to attract the
best students and post-docs.

My career has thus far run at an oblique angle to the normal career of a
university lecturer.  This has given me some freedom to expand the breadth of
my experience and collaborations.  It has also meant that I have not had a job
for which I needed grant funding.  The MRC CBU is a fully funded MRC unit, and
my position in Berkeley is paid for by the Brain Imaging Center.  My role in
Berkeley already involves collaborating with other psychologists, imaging
researchers, and physicists, and my work on open source software development
is a collaboration with a network of researchers in the US and Europe.  As a
result, I have neither needed nor applied for grant funding since the
successful RO1 grant in 2008, and an application to the 2010 challenge grant
series, for development of diffusion imaging.

I know that the best way to inform teaching and development is to share work
on research problems.  I plan to continue my current collaborations with
colleagues in Berkeley and Stanford on imaging methods, but it would enrich my
work to develop substantial scientific collaborations in Birmingham.  I
propose to start with collaborations on movement neuroscience.  I believe my
broad experience of FMRI and diffusion imaging would be useful for that
collaboration, and I would have the chance to go back to the applied research
problems that I have already worked on.  My background in neurology makes me
well-suited to work on the neuroscience of movement disorders such as apraxia
and Parkinson's disease.  I plan to continue my work on imaging statistics
with Professor Tom Nichols at Warwick and promote regional collaboration with
shared seminars and courses.

[1] http://software-carpentry.org/
[2] http://nipy.org
[3] http://vcresearch.berkeley.edu/datascience
[4] http://nipyworld.blogspot.com/2012/04/replication-in-functional-mri.html
[5] http://www.jarrodmillman.com/rcsds/
[6] https://bic-berkeley.github.io/psych-214-fall-2016
