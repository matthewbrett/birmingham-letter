Data analysis for the life sciences
===================================

*Data analysis* is a term from John Tukey's "The Future of Data Analysis"
(1962).  I am updating the term as used by Tukey to cover three related themes
that have appeared in reaction to the radical change in the role of computing
in scientific practice:

1. **data science** - closest in theme to Tukey's "data analysis", data
   science is a set of ideas centred around the need to teach students to a)
   explore and understand data and b) be flexible in adapting their analysis
   to their understanding of the data and c) understand the analysis in terms
   of algorithms implemented with code, in order to give better insight into
   modern analysis techniques, and greater freedom in designing new analyses;
1. **scientific computing** - working with computers involves code, data, and
   the relationship between them.  Haphazard and informal techniques of code
   development and data organization lead to errors and inefficiency; it is
   harder for other scientists to collaborate and reproduce the analysis.
   These problems can be reduced by training on modern tools and working
   practice (Wilson et al. "Best Practices for scientific computing" 2014).
1. **big data** - datasets may be too large to store or analyze on
   a single physical computer.  This adds complexity to the analysis and
   increases the risk of error.  To analyze these datasets, researchers need a
   solid foundation in data science and scientific computing.  Data science
   provides a background in algorithmic thinking, and scientific computing
   contains the ideas needed to work on clusters of computers and distributed
   databases.

## Responsibilities

* develop and teach courses on data analysis;
* collaborate with other schools in development of data analysis curricula;
* develop collaborations with other universities on course design and
  materials;
* teach graduate functional brain imaging analysis;
* supervise graduate students on functional brain imaging analysis;
* develop and maintain tools for data analysis in functional brain imaging.

## Strategy

This field is new to undergraduates in its combination of subject matter and
methods of teaching.  Other major universities that lead in this field are
still developing their course structure and teaching materials.  To be useful,
Birmingham data analysis courses will have to work together with research
methods courses in the life sciences:

* pilot undergraduate data analysis course with materials based on open
  textbook and tools from the Berkeley data science course, and related
  courses applying data science methods to analysis problems from specific
  fields;
* study current research methods classes across life sciences;
* run class for other teachers to show data analysis teaching methods, and to
  encourage collaborations on subject-specific extensions of these methods;
* expand data analysis course, add parallel subject-specific courses building
  on data analysis methods taught in the main course;
* collaborate with other schools to develop common materials;
* update and inform data analysis knowledge by application to brain imaging
  analysis.

## References

Donoho, David L. "50 years of Data Science." Princeton NJ, Tukey Centennial
Workshop. 2015.

Tukey, John W. "The Future of Data Analysis." Ann. Math. Statist. 33 (1962),
no.  1, 1--67. doi:10.1214/aoms/1177704711.
http://projecteuclid.org/euclid.aoms/1177704711.

Wilson, Greg, et al. "Best practices for scientific computing." PLoS Biol 12.1
(2014): e1001745.
