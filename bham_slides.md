% Teaching and research plan
% Matthew Brett
% January 30th 2017

# Teaching - imaging

How do we learn physics, signal processing, image processing, linear algebra,
statistics, anatomy, computing?

How can we collaborate with, learn from physicists, engineers, statisticians,
programmers?

Faced with such complexity, how do we defend ourselves against error,
confirmation bias and post-hoc hypotheses?

# Two modes

* black box (cost: disengagement, garbage in, gospel out, benefit: fast
  output);
* "What I cannot create, I do not understand" (Feynman); (cost: time in
  curriculum, commitment from students, longer time to first paper; benefit:
  skepticism, long-term learning, creativity);

# Teaching for understanding

* understanding one level down;
* find the tractable mathematics, prove it, use it;
* teach algorithms with code;
* understanding by building.

# Control complexity with process

* collaboration;
* documentation;
* testing;
* version control;
* continuous integration;
* reproducibility.

# Berkeley functional MRI methods course

> This is a hands-on course teaching the principles of functional MRI (fMRI)
> data analysis. We will teach you **how to work with data and code to get a
> deeper understanding of how fMRI methods work, how they can fail, how to fix
> them, and how to develop new methods.** We will cover the basic concepts in
> neuroimaging analysis, and how they relate to the wider world of statistics,
> engineering and computer science. At the same time we will teach you
> **techniques of data analysis that will make your work easier to organize,
> understand, explain and share.** At the end of the course we expect you to
> be able to analyze fMRI data using Python and keep track of your work with
> version control using git.  [course
> website](https://bic-berkeley.github.io/psych-214-fall-2016);

# Syllabus

* nature of imaging data;
* analysis concepts;
* collaboration, correctness and reproducibility.

See: [course
syllabus](https://bic-berkeley.github.io/psych-214-fall-2016/syllabus.html)

# Reproducible projects

\centerline{\includegraphics[width=4.5in]{red_home.png}}

See: [red project
website](https://github.com/psych-214-fall-2016/project-red).

# Outcome

> However, writing and testing code from scratch gave us a much better
> understanding of what the pipelines do, and it underscored the complexity of
> these steps beyond the basic hand-wavy/intuitive ideas. Our main takeaway is
> that it's important to inspect analysis stages and not just accept final
> results.

From "Discussion" at [red project website](https://github.com/psych-214-fall-2016/project-red).

# Outcome

> One of my main takeaways from this course has been in **applying the math,
> stats, and coding back to these analyses to make more informed decisions and
> bolster my understanding of how and why I would take certain steps to answer
> specific questions** about the fMRI data in question.

# Berkeley data science course

Collaboration of computer science and statistics departments.

* understanding data and algorithms through code;
* [Inferential thinking textbook](https://www.inferentialthinking.com);
* for example - a beautiful [walk-through implementing a classification
  algorithm](https://www.inferentialthinking.com/chapters/15/classification.html).

# Outreach

* EdX course

# Research plan

* constrained regression for modeling FMRI noise;
* customizing brain parcellation;
* talking to you...

# Constrained regression - background

* automating models for noisy scans

\newcommand{\yvec}{\vec{y}}
\newcommand{\xvec}{\vec{x}}
\newcommand{\evec}{\vec{\varepsilon}}
\newcommand{\bvec}{\vec{\beta}}

Multiple regression:

\begin{equation*}
y_i = b_1 x_{1, i} + b_2 x_{2, i} + ... b_p x_{p, i} + e_i
\end{equation*}

In matrix form:

\begin{equation*}
Y = X B + E
\end{equation*}

# Original design

$X_o$:

\centerline{\includegraphics[height=3.25in]{design_illustration.png}}

# Least squares estimation

$$
\hat{B} = \text{minimize}_{B} \frac{1}{2} \|Y - X B \|^2_F
$$

$$
\hat{E} = Y - X \hat{B}
$$

# Least squares estimation - structured residuals

$$
\hat{E}
$$

\centerline{\includegraphics[width=5.5in]{powers_plot.png}}

# Partial constrained regression

$$
\begin{bmatrix}
1 & 0 & 0 & ... & 0 \\
0 & 1 & 0 & ... & 0 \\
0 & 0 & 1 & ... & 0 \\
\vdots & \vdots & \vdots & \ddots & \vdots \\
0 & 0 & 0 & ... & 1
\end{bmatrix}
$$

# Partial constrained regression

\centerline{\includegraphics[width=5in]{x_a.png}}

# Partial constrained regression

\begin{equation*}
B_a = \text{minimize}_{B_o, B_c} \frac{1}{2} \|Y - X_o B_o - X_c B_c\|^2_F + \lambda
\sum_{g \in G} w_g \|B_c[g]\|_2
\end{equation*}

* $X_o$ : original design;
* $B_o$ : parameters for columns of original design;
* $w_g$ : weight for column $g$ of confound design.
* $X_c$ : confound design;
* $B_c$ : parameters for columns of confound design;
* $\lambda$ : weighting parameter for penalizing $B_c$.

# Constrained regression - plan

* optimized estimation of original design with outlier regression;
* inference after model selection;
* compare recovery of activation patterns with outlier deletion, regression
  against other outlier removal methods.
* already available are:

    * [regreg](https://regreg.github.io/regreg) - a Python toolbox for setting
      up / estimating regularized regression problems;
    * Fithian, Sun & Taylor (2015) "Optimal Inference After Model Selection"
      [arXiv:1410.2597v2 \[math.ST\]](https://arxiv.org/abs/1410.2597v2);

* many general applications;

# Automatic parcellation

* comparing between individuals, studies, species:
  * replication;
* increasing power by selective averaging;

Brett, Johnsrude & Owen (2002) [The problem of functional localization in the
human brain](http://matthew.dynevor.org/research/articles/location.pdf) Nat
Rev Neurosci, 3(3):243–249.

Saxe, Brett and Kanwisher (2006) [Divide and conquer: a defense of functional
localizers](http://dx.doi.org/10.1016/j.neuroimage.2005.12.062) Neuroimage,
30(4):1088–96.

# Human Connectome Project parcellation

\centerline{\includegraphics[width=4.5in]{glasser_parcellation_55b.png}}

Largely automatic and data-driven.

Glasser et al (2016) [A multi-modal parcellation of human cerebral
cortex](http://www.nature.com/nature/journal/v536/n7615/full/nature18933.html)
Nature 536, 171–178.

# The parcellation

\centerline{\includegraphics[width=4.5in]{glasser_parcellation_map.png}}

# Parcellation problems

* great complexity of method;
* HCP data highly specific - how does it apply to my data?
* parcellation more accurate for group than individual - how does it apply to
  my subject?
* does the parcellation make sense for my area of interest and application?

# Research plan

\centerline{\includegraphics[width=4.5in]{parcellation.pdf}}

# Development plan

* bridge across fields from neurology through neuroscience, statistics, data
  science, computing;
* increase collaboration with other methodologists:
    * Tom Nichols (Warwick);
* continue development of software building blocks for imaging tools.

# The end

Thanks for your attention

# Diffusion for parcellation

\centerline{\includegraphics[width=4.5in]{johansen_berg_sma.png}}
