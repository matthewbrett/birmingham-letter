.. You may wish to prepare this section in a word document and then paste the
   text from word into the other information field below. Please note this
   should be presented in plain text as any formatting such as bold,
   underline, bullet points etc, will be lost. This section should set out your
   reasons for applying, skills and experience, other interests and activities
   as appropriate to the post for which you are applying. For academic posts,
   you should provide a list of your publications in a format which accords
   with the norms for your discipline.

.. This section of the form gives you an opportunity to show how your past
   experience, both in employment and in other areas of your life, has given
   you the necessary skills to be successful in the post you have applied for.
   Look carefully at the Job Description and Person Specification for the post
   and try to demonstrate, with examples of what you have done, that you are a
   suitable candidate for the post.

.. Description

   Skills and Experience – Senior Lecturer

   Demonstrated competence in Research; Learning and Teaching; and Management and
   Administration; and excellence in at least two of these areas.

   Research requirements

   An excellent national reputation and a developing international profile through
   significant original research work and a clear record of impact. Evidence of
   success under the following headings, as appropriate to the discipline:

   * High level peer esteem as evidenced by

     * Excellent reputation in the UK and often internationally, reflected in
       sustained high quality output, level of innovation, impact on subject
       and recognition;
     * An excellent and sustained record of peer reviewed research
       publications;

   * Successful and sustained supervision of doctoral students to completion;
   * Substantial and sustained research income generation, e.g. through
     research grants, contracts, research consultancy or other external
     funding;
   * Sustained high value impact knowledge transfer and enterprise that is of
     manifest benefit to the College and University.

   Learning and teaching requirements

   An excellent teaching profile and performance in terms of both impact and
   quality. The teaching quality demonstrated to be informed by an appropriate
   level of scholarship. Evidence of success under the following headings:

   * High national reputation for the development of teaching and learning
     excellence within the discipline;
   * Successful and sustained use of a range of appropriate teaching methods,
     and assessment strategies that promote high quality learning, including
     learning that is flexible, distinctive and current and stimulates
     learners’ natural curiosity;
   * Significant and sustained contribution to one or more of the following:
     strategic development of new programmes; approaches to learning; the
     development of learning resources;
   * High quality and sustained contributions to fostering excellence in
     teaching activities more widely, i.e. in the Department/School or College
     and/or externally;
   * Track record of substantial and sustained high value impact on the
     enhancement of the student experience, and/or employability;
   * Mentoring and expert advice which develops the skills of colleagues in
     teaching and in fostering learning.

   Management and administration-related requirements

   Demonstrated significant achievement in management and
   administration-related activities, which may include leadership of
   activities/initiatives. Evidence of success under the following headings:

   * Successful and sustained performance in significant
     administrative/managerial role (s) (e.g. exams officer);
   * Significant and sustained high quality innovative contributions to the
     management/administration of the Department/School/College or University;
   * Successful and sustained contribution to the corporate life of the
     School/College/University, displaying willingness to contribute actively
     to committees, collaborative teaching and administrative tasks.
